﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace LesVoorbeeldAttributeRouting.Controllers
{
    [Route("")]
    public class StudentController : Controller
    {
        [Route("")]
        [Route("Student/Index/{naam:length(3,15)}/{leeftijd:int:min(18)}")]
        public IActionResult Index(string naam, int leeftijd=0)
        {
            if (!string.IsNullOrEmpty(naam))
            {
                return  Content($"{naam} is {leeftijd} jaar oud");
            }
            return View();
        }

        [Route("[controller]/[action]")]
        public IActionResult Add()
        {
            return Content("Added");
        }

    }


}
